//
//  FirService.swift
//  DemoGeoFire
//
//  Created by vmio vmio on 5/6/19.
//  Copyright © 2019 LoginChatApp. All rights reserved.
//

import FirebaseAuth
import FirebaseDatabase
import Firebase
import FirebaseStorage
import GeoFire

class FirService {
    
    static let sharedInstance: FirService = FirService()
    
    private init(){}
    
    func setUserLocation(location: CLLocation, geofire: GeoFire) {
        let uid = Auth.auth().currentUser?.uid
        geofire.setLocation(location, forKey: uid!)
    }
    
    func getLocation(geofire: GeoFire) -> CLLocation? {
        var loc: CLLocation?
        if let uid = Auth.auth().currentUser?.uid {
            geofire.getLocationForKey(uid, withCallback: { (location, err) in
                if err != nil {
                    print( "Error getting Userlocation from Geofire \(err.debugDescription)")
                } else {
                    print( "UserLocation is available \(location.debugDescription)")
                    loc = location
                }
            })
        }
        return loc
    }

}
