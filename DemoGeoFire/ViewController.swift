//
//  ViewController.swift
//  DemoGeoFire
//
//  Created by vmio vmio on 5/6/19.
//  Copyright © 2019 LoginChatApp. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import CoreLocation
import GeoFire
import Pastel
class ViewController: UIViewController {

 
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var passTextField: UITextField!
    @IBOutlet var pastelView: PastelView!
    
    let locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    override func viewDidLoad() {
        super.viewDidLoad()
        currentLocation = locationManager.location
        
        pastelView.startPastelPoint = .bottomLeft
        pastelView.endPastelPoint = .topRight
        pastelView.animationDuration = 3.0
        pastelView.setColors([UIColor(red: 156/255, green: 39/255, blue: 176/255, alpha: 1.0),
                              UIColor(red: 255/255, green: 64/255, blue: 129/255, alpha: 1.0),
                              UIColor(red: 123/255, green: 31/255, blue: 162/255, alpha: 1.0),
                              UIColor(red: 32/255, green: 76/255, blue: 255/255, alpha: 1.0),
                              UIColor(red: 32/255, green: 158/255, blue: 255/255, alpha: 1.0),
                              UIColor(red: 90/255, green: 120/255, blue: 127/255, alpha: 1.0),
                              UIColor(red: 58/255, green: 255/255, blue: 217/255, alpha: 1.0)])
        pastelView.startAnimation()
    
    }

    @IBAction func loginButton(_ sender: Any) {
        guard let email = nameTextField.text, let pass = passTextField.text else {
            print("nil")
            return
        }

        Auth.auth().signIn(withEmail: email, password: pass) { (user, error) in
            if error != nil {
        
                let alert = UIAlertController(title: "Error", message: error.unsafelyUnwrapped.localizedDescription, preferredStyle: .alert)
                let openAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                alert.addAction(openAction)
                self.present(alert, animated: true, completion: nil)
                return
            }
            self.setupLocationManager()
            print("Login Successfuly")

        }
        
        let vc = ViewPhotoController()
        present(vc, animated: true, completion: nil)
//        register()
    }
    
    func register() {
        guard let email = nameTextField.text, let pass = passTextField.text else {
            print("nil")
            return
        }
        
        Auth.auth().createUser(withEmail: email, password: pass) { (user, error) in
            if error != nil {
                print(error ?? "")
                let alert = UIAlertController(title: "Error", message: error.unsafelyUnwrapped.localizedDescription, preferredStyle: .alert)
                let openAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                alert.addAction(openAction)
                self.present(alert, animated: true, completion: nil)
                return
            }
            
            let ref = Database.database().reference()
            let userReference = ref.child("Users").child(user!.user.uid)
            let values = ["email": email]
            userReference.updateChildValues(values, withCompletionBlock: { (error, ref) in
                
                if error != nil {
                    print(error ?? "")
                    return
                }
                
                self.setupLocationManager()
                print("Save User successfully into firebase")
            })
        }
    }
}

extension ViewController : CLLocationManagerDelegate {
    //MARK: - Location related methods
    
    func setupLocationManager() {
 
        let uid = Auth.auth().currentUser?.uid
        let LOCATION = Database.database().reference().child("User Location").child(uid!)
        let GEO_REF = GeoFire(firebaseRef: LOCATION)
        
        if FirService.sharedInstance.getLocation(geofire: GEO_REF) == nil {
            self.isAuthorizedtoGetUserLocation()
            if CLLocationManager.locationServicesEnabled() {
                self.locationManager.delegate = self
                self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
                self.locationManager.startUpdatingLocation() //this will invoke the didUpdateLocations method
            }
        }
        
      
    }
    
    func isAuthorizedtoGetUserLocation() {
        
        if CLLocationManager.authorizationStatus() != .authorizedWhenInUse {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations[locations.count - 1]
        if location.horizontalAccuracy > 0 {
            locationManager.stopUpdatingLocation() //got the location and now we can stop updating location.
        
        }
        let uid = Auth.auth().currentUser?.uid
        let LOCATION = Database.database().reference().child("User Location").child(uid!)
        let GEO_REF = GeoFire(firebaseRef: LOCATION)
 
        FirService.sharedInstance.setUserLocation(location: location, geofire: GEO_REF)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error getting location \(error)")
    }
}
